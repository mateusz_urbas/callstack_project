# Callstack's recruitment app

## Getting started

- install npm dependencies - `yarn`
- install pods - `pod install` in ios directory
- run the app - `yarn ios`/`yarn android`

## Implementation

TODO: your notes and explanation here

- Fix bugs:
  - used ICON_SIZE in 'Cell.tsx' file,
- Transitioning a React Native app to TypeScript,
- Adding CDS (CallStack Design System). Contains:
  - reusable components for all project (may contains e.g. Buttons, Inputs, etc... ),
  - constants for components (colors, etc...),
